# -*- coding: latin-1 -*-

'''----------------------------------------------------------------------------
CalculNAssign

-------------------------------------------------------------------------------
Écrit par  : Louis-Philippe Valiquette et Kevin Ratelle
Créé le    : September 16, 2015
----------------------------------------------------------------------------'''
''' Bibliotheque qui va storer le nombre de fois que les gens
sont venus et combien ils ont déboursés jusqu'à maintenant en
plus de leur moyenne de déboursé en fonction de leur présence '''

from CVSFileHandler import CSVReader

cvs_handler = CSVReader('Dictionnaire.cvs')
DictArray = cvs_handler.get_transaction_dict()

# On prend les presences
Participants = input('Presences: ')
Participants = Participants.split( )

for i in range(cvs_handler.number_of_people):
	if DictArray[i]['Nom'] in Participants:
		DictArray[i]['Presences'] += 1

# La biere
CoutBiere = int(input('Combien coute la biere: '))

AcheteurBiere = input('Qui a paye la biere: ')

for i in range(cvs_handler.number_of_people):
        if DictArray[i]['Nom'] in AcheteurBiere:
                DictArray[i]['TotalPaye'] += CoutBiere
                break
	
# Qui va payer
Cout = int(input('Combien ça coûte: '))

# On va ordonner par total paye, puis sortir le premier present
DictArray = sorted(DictArray, key=lambda k: k['TotalPaye']/k['Presences']) 
for i in range(cvs_handler.number_of_people):
	if (DictArray[i]['Nom'] in Participants):
		print(DictArray[i]['Nom'] + ' va payer.')
		DictArray[i]['TotalPaye'] += Cout
		break

# Distrubution des couts, je ne suis pas encore certain que ça va être nécessaire, mais je l'ai déjà codé
'''
CoutParPersonne = int(input('Coût arrondi: ')) / len(Participants)
if 'LP' in Participants:
    LP['MoyCouts'] = int((LP['MoyCouts'] * (LP['Presences'] - 1) + CoutParPersonne) / LP['Presences'])
if 'Sam' in Participants:
    Sam['MoyCouts'] = int((Sam['MoyCouts'] * (Sam['Presences'] - 1) + CoutParPersonne) / Sam['Presences'])
if 'Simon' in Participants:
    Simon['MoyCouts'] = int((Simon['MoyCouts'] * (Simon['Presences'] - 1) + CoutParPersonne) / Simon['Presences'])
if 'Kev' in Participants:
    Kev['MoyCouts'] = int((Kev['MoyCouts'] * (Kev['Presences'] - 1) + CoutParPersonne) / Kev['Presences'])
if 'Jean' in Participants:
    Jean['MoyCouts'] = int((Jean['MoyCouts'] * (Jean['Presences'] - 1) + CoutParPersonne) / Jean['Presences'])
'''

cvs_handler.set_transaction_dict(DictArray)
cvs_handler.printToFile()
#print(DictArray)

input('Bon souper!')

Ce projet à été codé sur PyCharm, mais ne devrait contenir que des fonctions de base disponibles
dans toutes les distro et IDE en python

Calculs et assignations contient les calculs à effectuer et trouve la personne qui doit payer et
update le fichier de dictionnaire des participants

Dictionnaire des participants contient les dictionnaires définissant toutes les données de chaque
participant sous trois catégories : Nombre de présences, Coûts payés jusqu'à maintenant et la
moyenne des coûts qu'elle a consommé.


À FAIRE :
- Monter le read et le write entre les deux fichiers
- Ajouter une procédure qui ajoute la bière achetée au montant dépensé à ce jour
- Faire en sorte qu'on puisse ajouter des participants au dictionnaire des participants juste en
    prenant les présences (quand on entre une présence qui n'a pas de dict, demander si on veut
    créer le dictionnaire associé à ce nom et l'initialiser à 0)

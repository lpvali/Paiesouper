''' Bibliotheque qui va storer le nombre de fois que les gens
sont venus et combien ils ont déboursés jusqu'à maintenant en
plus de leur moyenne de déboursé en fonction de leur présence '''

LP = {'Nom': 'LP', 'Presences': 0, 'TotalPaye': 0, 'MoyCouts': 0}
Sam = {'Nom': 'Sam', 'Presences': 0, 'TotalPaye': 0, 'MoyCouts': 0}
Simon = {'Nom': 'Simon', 'Presences': 0, 'TotalPaye': 0, 'MoyCouts': 0}
Kev = {'Nom': 'Kev', 'Presences': 0, 'TotalPaye': 0, 'MoyCouts': 0}
Jean = {'Nom': 'Jean', 'Presences': 0, 'TotalPaye': 0, 'MoyCouts': 0}
LP = eval(open('Dictionnaire.txt').readline(1))
Print(LP)


# On prend les presences
Participants = input('Presences: ')
Participants = Participants.split( )

if 'LP' in Participants:
    LP['Presences'] += 1

if 'Sam' in Participants:
    Sam['Presences'] += 1

if 'Simon' in Participants:
    Simon['Presences'] += 1

if 'Kev' in Participants:
    Kev['Presences'] += 1

if 'Jean' in Participants:
    Jean['Presences'] += 1

# Qui va payer

Cout = int(input('Combien ça coûte: '))

# On va utiliser une varible Score pour calculer à quel point le monde sont proche de payer

LPScore = 0
KevScore = 0
SimonScore = 0
SamScore = 0
JeanScore = 0

if 'LP' in Participants:
    LPScore = (LP['TotalPaye'] / LP['Presences'])
    if LPScore <= KevScore and LPScore <= SimonScore and LPScore <= JeanScore and LPScore <= SamScore:
        print('L-P doit payer')
        LP['TotalPaye'] = LP['TotalPaye'] + Cout
if 'Kev' in Participants:
    KevScore = (Kev['TotalPaye'] / Kev['Presences'])
    if KevScore <= LPScore and KevScore <= SimonScore and KevScore <= JeanScore and KevScore <= SamScore:
        print('Kev doit payer')
        Kev['TotalPaye'] = Kev['TotalPaye'] + Cout
if 'Simon' in Participants:
    SimonScore = (Simon['TotalPaye'] / Simon['Presences'])
    if SimonScore <= KevScore and SimonScore <= LPScore and SimonScore <= JeanScore and SimonScore <= SamScore:
        print('Simon doit payer')
        Simon['TotalPaye'] = Simon['TotalPaye'] + Cout
if 'Sam' in Participants:
    SamScore = (Sam['TotalPaye'] / Sam['Presences'])
    if SamScore <= KevScore and SamScore <= SimonScore and SamScore <= JeanScore and SamScore <= LPScore:
        print('Sam doit payer')
        Sam['TotalPaye'] = Sam['TotalPaye'] + Cout
if 'Jean' in Participants:
    JeanScore = (Jean['TotalPaye'] / Jean['Presences'])
    if JeanScore <= KevScore and JeanScore <= SimonScore and JeanScore <= LPScore and JeanScore <= SamScore:
        print('Jean doit payer')
        Jean['TotalPaye'] = Jean['TotalPaye'] + Cout


# Distrubution des couts, je ne suis pas encore certain que ça va être nécessaire, mais je l'ai déjà codé
'''
CoutParPersonne = int(input('Coût arrondi: ')) / len(Participants)

if 'LP' in Participants:
    LP['MoyCouts'] = int((LP['MoyCouts'] * (LP['Presences'] - 1) + CoutParPersonne) / LP['Presences'])

if 'Sam' in Participants:
    Sam['MoyCouts'] = int((Sam['MoyCouts'] * (Sam['Presences'] - 1) + CoutParPersonne) / Sam['Presences'])

if 'Simon' in Participants:
    Simon['MoyCouts'] = int((Simon['MoyCouts'] * (Simon['Presences'] - 1) + CoutParPersonne) / Simon['Presences'])

if 'Kev' in Participants:
    Kev['MoyCouts'] = int((Kev['MoyCouts'] * (Kev['Presences'] - 1) + CoutParPersonne) / Kev['Presences'])

if 'Jean' in Participants:
    Jean['MoyCouts'] = int((Jean['MoyCouts'] * (Jean['Presences'] - 1) + CoutParPersonne) / Jean['Presences'])
'''

#Test d'impression
print(LP)
print(Sam)
print(Simon)
print(Kev)
print(Jean)

print(len(Participants))

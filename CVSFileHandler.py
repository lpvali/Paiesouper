# -*- coding: latin-1 -*-
'''----------------------------------------------------------------------------
CVS File Handler

-------------------------------------------------------------------------------
Écrit par  : Kevin Ratelle
Créé le    : September 16, 2015
----------------------------------------------------------------------------'''

import csv
from os import curdir, sep

class CSVReader:
    
	# Read/write methods ------------------------------------
	def __init__(self, transaction_file):
		self.transaction_file = transaction_file
		self.read_transaction_file(transaction_file)
		self.convertStringToNumber()
	
	def printToFile(self):
		self.convertNumberToString()
		try:
			with open(self.transaction_file, 'w', newline='') as f:
				writer = csv.DictWriter(f, delimiter=';', fieldnames=sorted(list(self.TransactionDict[0].keys())))
				writer.writeheader()
				for dict in self.TransactionDict:
					writer.writerow(dict)
		except:
			print('Something went wrong ! (Simon must pay)')
	
	def read_transaction_file(self, transaction_file):
		self.number_of_people = 0
		f = open(transaction_file)
		
		try:
			Dict = csv.DictReader(f, delimiter=';')
			self.TransactionDict = self.clean_dict(Dict)
			self.number_of_people = len(self.TransactionDict)

		finally:
			f.close()
			
	# set/get methods ------------------------------
	def get_transaction_dict(self):
		return self.TransactionDict
	
	def set_transaction_dict(self, DictArray):
		self.TransactionDict = DictArray
		
	# support methods (utils)
	def convertStringToNumber(self):
		for i in range(self.number_of_people):
			self.TransactionDict[i]['Presences'] = int(self.TransactionDict[i]['Presences'])
			self.TransactionDict[i]['TotalPaye'] = float(self.TransactionDict[i]['TotalPaye'])
			
	def convertNumberToString(self):
		for i in range(self.number_of_people):
			self.TransactionDict[i]['Presences'] = str(self.TransactionDict[i]['Presences'])
			self.TransactionDict[i]['TotalPaye'] = str(self.TransactionDict[i]['TotalPaye'])
	
	def clean_dict(self, dict):
		''''''
		out_dict = []
		for row in dict:
			if row['Nom']!='':
				out_dict.append(row)
				
		return out_dict